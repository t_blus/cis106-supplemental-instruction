def get_int():
  try:
    inp = input("Enter an int: ")
    inp = int(inp)
  except Exception as ex:
    print("invalid input")
    print(ex)
  else:
    return inp
  return False

def print_person(name, height, weight):
  print(f"{name}...{height} inches...{weight} pounds")

def read_n_print(filename):
  count = 0
  with open(filename, "r") as f:
    for line in f:
      print(line.strip())
      count += 1
  print(f"there were {count} lines in {filename}")

def get_n_flip():
  string = input("Enter any string")
  print(string[::-1])

def calc_avg(array):
  return sum(array)/len(array)

def make_array():
  array = []
  for i in range(10):
    array.append(2**i)
  return array

def main():
  inte = get_int()
  print(inte)
  get_n_flip()
  print_person("Tristan Blus", 73, 180)
  read_n_print("temp.temp")
  array = make_array()
  print(array)
  print(calc_avg(array))


main()

